module DefaultPageContent
  extend ActiveSupport::Concern

  included do
    before_filter :set_page_defaults
  end

  def set_page_defaults
    @page_title = "Bekhruz Mirtursinov"
    @seo_keywords = "Bekhruz's Portfolio"
  end
end