module DefaultPageContent
  extend ActiveSupport::Concern

  included do
    before_filter :set_page_defaults
  end

  def set_page_defaults
    @page_title = "Bekhruz Mirtursinov | My Portfolio Website"
    @seo_keywords = "Bekhruz Mirtursinov portfolio"
  end
end